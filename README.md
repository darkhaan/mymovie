# Mymovie

![](board.png)

## Installation

- Clone this repository
- Go to inside folder `cd mymovie`
- Create .env file from example `cp .env.example .env`
- Edit .env file `vim .env`
- Run `docker-compose up`
- Run all migrations `docker-compose exec mymovie migrate`
- Load data `docker-compose exec mymovie load_data`
