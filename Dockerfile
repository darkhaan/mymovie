FROM python:3.7-slim

ENV PYTHONUNBUFFERED 1
ENV PYTHONPATH /proj
ENV PYTHONIOENCODING utf-8

RUN apt-get update && \
    apt-get install -y gcc git gettext curl wget && \
    apt-get install -yqq unzip && apt-get install -y build-essential && apt clean && rm -rf /var/cache/apt/*
RUN pip install pipenv

COPY Pipfile .
COPY Pipfile.lock .
RUN PIPENV_VENV_IN_PROJECT=1 pipenv install --system --deploy

RUN useradd -m -d /proj -s /bin/bash app
COPY . /proj
WORKDIR /proj
RUN chmod +x /proj/bin/*
ENV PATH "$PATH:/proj/bin"
RUN chown -R app:app /proj/*

USER app
