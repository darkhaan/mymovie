import os

from dotenv import load_dotenv

load_dotenv()


class Settings:
    APP_NAME = os.getenv('APP_NAME', 'mymovie')
    HOST = os.getenv('HOST', '0.0.0.0')
    PORT = os.getenv('PORT', 3000)
    DEBUG = os.getenv('DEBUG', True)
    SQLALCHEMY_DATABASE_URI = os.getenv('DATABASE_URL')
