from typing import Dict


class UseCase:

    def execute(self, dto: Dict) -> Dict:
        ...
