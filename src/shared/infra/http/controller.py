from typing import Dict

from flask import Request


class BaseController:

    def execute(self, request: Request) -> Dict:
        return self.execute_impl(request)

    def execute_impl(self, request: Request) -> Dict:
        ...
