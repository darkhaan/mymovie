from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref

from .base import Base
from .movie import Movie

metadata = Base.metadata


class Genre(Base):
    __tablename__ = 'genre'

    id = Column(Integer, primary_key=True)
    name = Column(String(1))

    movies = relationship('GenreMovie', back_populates='genre')


class GenreMovie(Base):
    __tablename__ = 'genre_movie'

    id = Column(Integer, primary_key=True)
    genre_id = Column(Integer, ForeignKey('genre.id'))
    movie_id = Column(Integer, ForeignKey('movie.id'))

    genre = relationship('Genre', back_populates='movies')
    movie = relationship('Movie', back_populates='genres')
