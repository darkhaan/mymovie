from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship, backref

from .base import Base
from .movie import Movie

metadata = Base.metadata


class Actor(Base):
    __tablename__ = 'actor'

    id = Column(Integer, primary_key=True)
    name = Column(String(1))

    movies = relationship('ActorMovie', back_populates='actor')


class ActorMovie(Base):
    __tablename__ = 'actor_movie'

    id = Column(Integer, primary_key=True)
    actor_id = Column(Integer, ForeignKey('actor.id'))
    movie_id = Column(Integer, ForeignKey('movie.id'))

    actor = relationship('Actor', back_populates='movies')
    movie = relationship('Movie', back_populates='actors')
