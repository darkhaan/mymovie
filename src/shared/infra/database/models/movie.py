from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from .base import Base

metadata = Base.metadata


class Movie(Base):
    __tablename__ = 'movie'

    id = Column(Integer, primary_key=True)
    title = Column(String(1))
    year = Column(Integer)

    actors = relationship('ActorMovie', back_populates='movie')
    genres = relationship('GenreMovie', back_populates='movie')
