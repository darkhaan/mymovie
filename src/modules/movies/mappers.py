from flask_marshmallow import Schema as Mapper
from marshmallow import fields


class ActorMap(Mapper):
    id = fields.Integer()
    name = fields.Str()


class GenreMap(Mapper):
    id = fields.Integer()
    name = fields.Str()


class ActorMovieMap(Mapper):
    actor = fields.Nested(ActorMap)


class GenreMovieMap(Mapper):
    genre = fields.Nested(GenreMap)


class MovieMap(Mapper):
    id = fields.Integer()
    title = fields.Str()
    year = fields.Integer()
    cast = fields.Nested(ActorMovieMap, attribute='actors', many=True)
    genres = fields.Nested(GenreMovieMap, many=True)


class ActorStatsMap(Mapper):
    id = fields.Integer()
    name = fields.Str()
    year = fields.Integer()
    movies_count = fields.Integer()
