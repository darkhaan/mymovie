from typing import Dict

from src.modules.movies.repositories import MovieRepository, ActorRepository
from src.shared.core.use_case import UseCase


class GetMoviesUseCase(UseCase):

    def __init__(self, repository: MovieRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        return self.repository.get_movies(dto['page'], dto['limit'])


class GetMovieUseCase(UseCase):

    def __init__(self, repository: MovieRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        return self.repository.get_movie_by_id(dto['movie_id'])


class CreateMovieUseCase(UseCase):

    def __init__(self, repository: MovieRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        return self.repository.create_movie(dto)


class EditMovieUseCase(UseCase):

    def __init__(self, repository: MovieRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        movie_id = dto.pop('movie_id')
        return self.repository.update_movie(movie_id, dto)


class DeleteMovieUseCase(UseCase):

    def __init__(self, repository: MovieRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        return self.repository.delete_movie(dto['movie_id'])


class GetActorsUseCase(UseCase):

    def __init__(self, repository: ActorRepository):
        self.repository = repository

    def execute(self, dto: Dict) -> Dict:
        return self.repository.get_actors()
