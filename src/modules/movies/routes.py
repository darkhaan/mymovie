from typing import Tuple

from flask import Blueprint, request, Response, jsonify

from src.modules.movies.controllers import (
    GetMoviesController,
    GetMovieController,
    CreateMovieController,
    EditMovieController,
    DeleteMovieController,
    GetActorsController,
)
from src.modules.movies.exceptions import MovieDoesNotExistException
from src.modules.movies.repositories import (
    SqlAlchemyMovieRepository,
    SqlAlchemyActorRepository,
)
from src.modules.movies.use_cases import (
    GetMoviesUseCase,
    GetMovieUseCase,
    CreateMovieUseCase,
    EditMovieUseCase,
    DeleteMovieUseCase,
    GetActorsUseCase,
)

blueprint = Blueprint('movies', __name__)

movie_repository = SqlAlchemyMovieRepository()
actor_repository = SqlAlchemyActorRepository()


@blueprint.route('/api/movies', methods=['GET'])
def get_movies() -> Tuple[Response, int]:
    use_case = GetMoviesUseCase(movie_repository)
    controller = GetMoviesController(use_case)

    try:
        result = controller.execute(request)
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 200


@blueprint.route('/api/movies/<movie_id>', methods=['GET'])
def get_movie(movie_id: int) -> Tuple[Response, int]:
    use_case = GetMovieUseCase(movie_repository)
    controller = GetMovieController(use_case)

    try:
        result = controller.execute(request)
    except MovieDoesNotExistException as e:
        return jsonify({
            'message': str(e)
        }), 404
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 200


@blueprint.route('/api/movies', methods=['POST'])
def create_movie() -> Tuple[Response, int]:
    use_case = CreateMovieUseCase(movie_repository)
    controller = CreateMovieController(use_case)

    try:
        result = controller.execute(request)
    except MovieDoesNotExistException as e:
        return jsonify({
            'message': str(e)
        }), 404
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 201


@blueprint.route('/api/movies/<movie_id>', methods=['PATCH'])
def edit_movie(movie_id: int) -> Tuple[Response, int]:
    use_case = EditMovieUseCase(movie_repository)
    controller = EditMovieController(use_case)
    result = controller.execute(request)

    try:
        result = controller.execute(request)
    except MovieDoesNotExistException as e:
        return jsonify({
            'message': str(e)
        }), 404
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 200


@blueprint.route('/api/movies/<movie_id>', methods=['DELETE'])
def delete_movie(movie_id: int) -> Tuple[Response, int]:
    use_case = DeleteMovieUseCase(movie_repository)
    controller = DeleteMovieController(use_case)

    try:
        result = controller.execute(request)
    except MovieDoesNotExistException as e:
        return jsonify({
            'message': str(e)
        }), 404
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 204


@blueprint.route('/api/actors', methods=['GET'])
def get_actors() -> Tuple[Response, int]:
    use_case = GetActorsUseCase(actor_repository)
    controller = GetActorsController(use_case)

    try:
        result = controller.execute(request)
    except Exception as e:
        return jsonify({
            'message': str(e)
        }), 400

    return jsonify(result), 200
