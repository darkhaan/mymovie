from marshmallow import Schema as DTO, fields


class GetMoviesDTO(DTO):
    page = fields.Integer()
    limit = fields.Integer()


class GetMovieDTO(DTO):
    movie_id = fields.Integer()


class CreateMovieDTO(DTO):
    title = fields.Str()
    year = fields.Integer()
    cast = fields.List(fields.Integer(), required=False)
    genres = fields.List(fields.Integer(), required=False)


class EditMovieDTO(DTO):
    movie_id = fields.Integer()
    title = fields.Str()
    year = fields.Integer()
    cast = fields.List(fields.Integer(), required=False)
    genres = fields.List(fields.Integer(), required=False)


class DeleteMovieDTO(DTO):
    movie_id = fields.Integer()
