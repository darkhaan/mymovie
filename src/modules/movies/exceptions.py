
class MovieDoesNotExistException(Exception):

    def __init__(self):
        message = 'Movie does not exist.'
        super().__init__(message)
