from typing import Dict, Tuple

from sqlalchemy import func
from sqlalchemy.orm import joinedload

from src.modules.movies.exceptions import MovieDoesNotExistException
from src.modules.movies.mappers import MovieMap, ActorStatsMap
from src.shared.infra.database.alchemy import db
from src.shared.infra.database.models import (
    Movie,
    Actor,
    ActorMovie,
    Genre,
    GenreMovie,
)

movie_mapper = MovieMap()
actor_mapper = ActorStatsMap()


class MovieRepository:

    def get_movies(self, page: int = 1, limit: int = 10) -> Dict:
        ...

    def get_movie_by_id(self, movie_id: int) -> Dict:
        ...

    def create_movie(self, dto: Dict) -> Dict:
        ...

    def update_movie(self, movie_id: int, dto: Dict) -> Dict:
        ...

    def delete_movie(self, movie_id: int) -> Dict:
        ...


class SqlAlchemyMovieRepository(MovieRepository):

    def get_movies(self, page: int = 1, limit: int = 10) -> Dict:
        query = db.session.query(Movie).options(
            joinedload('actors')).options(joinedload('genres')).paginate(
            page, limit)
        return movie_mapper.dump(query.items, many=True)

    def get_movie_by_id(self, movie_id: int) -> Dict:
        instance = self._get_movie_by_id(movie_id)
        return movie_mapper.dump(instance)

    def create_movie(self, dto: Dict) -> Dict:
        cast = tuple(dto.pop('cast'))
        genres = tuple(dto.pop('genres'))
        instance = Movie(**dto)

        self._append_related(instance, cast, genres)
        db.session.add(instance)
        db.session.commit()
        return movie_mapper.dump(instance)

    def update_movie(self, movie_id: int, dto: Dict) -> Dict:
        instance = self._get_movie_by_id(movie_id)

        cast = tuple(dto.pop('cast'))
        genres = tuple(dto.pop('genres'))

        for key, attr in dto.items():
            setattr(instance, key, attr)

        self._append_related(instance, cast, genres)
        db.session.add(instance)
        db.session.commit()
        return movie_mapper.dump(instance)

    def delete_movie(self, movie_id: int) -> Dict:
        instance = self._get_movie_by_id(movie_id)
        db.session.delete(instance)
        db.session.commit()
        return {}

    def _get_movie_by_id(self, movie_id: int) -> Movie:
        instance = db.session.query(Movie).filter(
            Movie.id == movie_id).first()
        if not instance:
            raise MovieDoesNotExistException

        return instance

    def _append_related(
        self,
        instance: Movie,
        cast: Tuple[int],
        genres: Tuple[int]
    ):
        cast = db.session.query(Actor).filter(
            Actor.id.in_(cast)).all()
        genres = db.session.query(Genre).filter(
            Genre.id.in_(genres)).all()

        for actor in cast:
            instance.actors.append(ActorMovie(actor=actor))

        for genre in genres:
            instance.genres.append(GenreMovie(genre=genre))


class ActorRepository:

    def get_actors(self) -> Dict:
        ...


class SqlAlchemyActorRepository(ActorRepository):

    def get_actors(self) -> Dict:
        actors = db.session.query(Actor).with_entities(
            Actor.id, Actor.name, Movie.year, func.count(Movie.id).label(
                'movies_count')).join(
            ActorMovie, ActorMovie.actor_id == Actor.id).join(
            Movie, Movie.id == ActorMovie.movie_id).group_by(
            Actor.id, Actor.name, Movie.year).order_by(
            Movie.year.desc(), 'movies_count').all()
        return actor_mapper.dump(actors, many=True)
