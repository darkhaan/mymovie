from typing import Dict

from flask import Request

from src.modules.movies.dtos import (
    GetMoviesDTO,
    GetMovieDTO,
    CreateMovieDTO,
    EditMovieDTO,
    DeleteMovieDTO,
)
from src.shared.core.use_case import UseCase
from src.shared.infra.http.controller import BaseController


class GetMoviesController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        dto = GetMoviesDTO().load({
            'page': request.args.get('page', 1),
            'limit': request.args.get('limit', 10),
        })
        return self.use_case.execute(dto)


class GetMovieController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        dto = GetMovieDTO().load({
            'movie_id': request.view_args.get('movie_id'),
        })
        return self.use_case.execute(dto)


class CreateMovieController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        request_data = request.get_json()
        dto = CreateMovieDTO().load({
            'title': request_data.get('title'),
            'year': request_data.get('year'),
            'cast': request_data.get('cast', []),
            'genres': request_data.get('genres', []),
        })
        return self.use_case.execute(dto)


class EditMovieController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        request_data = request.get_json()
        dto = EditMovieDTO().load({
            'movie_id': request.view_args.get('movie_id'),
            'title': request_data.get('title'),
            'year': request_data.get('year'),
            'cast': request_data.get('cast', []),
            'genres': request_data.get('genres', []),
        })
        return self.use_case.execute(dto)


class DeleteMovieController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        dto = DeleteMovieDTO().load({
            'movie_id': request.view_args.get('movie_id'),
        })
        return self.use_case.execute(dto)


class GetActorsController(BaseController):

    def __init__(self, use_case: UseCase):
        self.use_case = use_case

    def execute_impl(self, request: Request) -> Dict:
        return self.use_case.execute({})
