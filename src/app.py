from typing import Any

from flask import Flask

from src.modules.movies import routes
from src.shared.infra.database.alchemy import db
from src.settings import Settings


def get_application(settings=Settings) -> Flask:
    app = Flask(settings.APP_NAME)
    app.config.from_object(settings)
    return app


def register_database(app: Flask) -> Any:
    db.init_app(app)


def register_routes(app: Flask) -> Any:
    app.register_blueprint(routes.blueprint)


app = get_application()

register_database(app)
register_routes(app)

app.run(host=Settings.HOST, port=Settings.PORT, debug=Settings.DEBUG)
