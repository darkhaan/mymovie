import json
import os

from dotenv import load_dotenv
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.shared.infra.database.models import (
    Movie,
    Actor,
    ActorMovie,
    Genre,
    GenreMovie,
)

load_dotenv()

engine = create_engine(os.getenv('DATABASE_URL'))
DBSession = sessionmaker(bind=engine)
session = DBSession()

with open('/proj/data/movies.json', 'rb') as file:
    data = json.load(file)
    movies = []
    for idx, item in enumerate(data):
        print('Loading item {} / {} ...'.format(idx, len(data)))

        movie = Movie(
            title=item['title'],
            year=item['year']
        )

        for actor_name in item['cast']:
            actor = session.query(Actor).filter(
                Actor.name == actor_name).first()
            if not actor:
                actor = Actor(name=actor_name)

            movie.actors.append(ActorMovie(actor=actor))

        for genre_name in item['genres']:
            genre = session.query(Genre).filter(
                Genre.name == genre_name).first()
            if not genre:
                genre = Genre(name=genre_name)

            movie.genres.append(GenreMovie(genre=genre))

        movies.append(movie)

    session.add_all(movies)
    session.commit()
